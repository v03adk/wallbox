<?php

declare(strict_types = 1);

namespace Kata\Domain;

use Kata\Domain\Orientation\OrientationInterface;

class Coordinates
{
    private $x;
    private $y;

    public function __construct(int $x, int $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    public function getX() : int
    {
        return $this->x;
    }

    public function getY() : int
    {
        return $this->y;
    }

    public function getNextCoordinatesAccordingToOrientation(OrientationInterface $orientation) : Coordinates
    {
        switch ($orientation->getRepresentation()) {
            case OrientationInterface::ORIENTATION_NORTH:
                return new Coordinates($this->x, $this->y + 1);
            case OrientationInterface::ORIENTATION_EAST:
                return new Coordinates($this->x + 1, $this->y);
            case OrientationInterface::ORIENTATION_SOUTH:
                return new Coordinates($this->x, $this->y - 1);
            case OrientationInterface::ORIENTATION_WEST:
                return new Coordinates($this->x - 1, $this->y);
        }
    }

    public function getRepresentation() : string
    {
        return sprintf('%s %s', $this->x, $this->y);
    }
}
