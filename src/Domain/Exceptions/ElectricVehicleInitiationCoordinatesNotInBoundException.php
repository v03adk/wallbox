<?php

declare(strict_types = 1);

namespace Kata\Domain\Exceptions;

class ElectricVehicleInitiationCoordinatesNotInBoundException extends \Exception
{
    protected $message = 'EV could not be initiated at this coordinates: is not in bound';
}