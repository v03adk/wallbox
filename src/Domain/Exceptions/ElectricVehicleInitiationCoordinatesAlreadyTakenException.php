<?php

declare(strict_types = 1);

namespace Kata\Domain\Exceptions;

class ElectricVehicleInitiationCoordinatesAlreadyTakenException extends \Exception
{
    protected $message = 'EV could not be initiated at this coordinates: already taken';
}