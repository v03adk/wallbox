<?php

declare(strict_types = 1);

namespace Kata\Domain\Exceptions;

class OrientationCreateInvalidRepresentationException extends \Exception
{
    protected $message = 'Invalid representation to create an orientation';
}