<?php

declare(strict_types = 1);

namespace Kata\Domain\Exceptions;

class ElectricVehicleMoveForwardCoordinatesAlreadyTakenException extends \Exception
{
    protected $message = 'EV could not be moved to this coordinates: already taken';
}