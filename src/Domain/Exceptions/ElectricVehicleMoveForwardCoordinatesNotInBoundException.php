<?php

declare(strict_types = 1);

namespace Kata\Domain\Exceptions;

class ElectricVehicleMoveForwardCoordinatesNotInBoundException extends \Exception
{
    protected $message = 'EV could not be moved to this coordinates: is not in bound';
}