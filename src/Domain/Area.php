<?php

declare(strict_types = 1);

namespace Kata\Domain;

class Area
{
    /** @var Coordinates */
    private $startCoordinates;
    /** @var Coordinates */
    private $endCoordinates;
    /** @var array */
    private $takenCoordinates;

    public function __construct(Coordinates $endCoordinates)
    {
        $this->startCoordinates = new Coordinates(0, 0);
        $this->endCoordinates = $endCoordinates;
        $this->takenCoordinates = [];
    }

    public function isCoordinatesInBound(Coordinates $coordinates) : bool
    {
        if ($coordinates->getX() >= $this->startCoordinates->getX() && $coordinates->getX() <= $this->endCoordinates->getX()
            && $coordinates->getY() >= $this->startCoordinates->getY() && $coordinates->getY() <= $this->endCoordinates->getY()) {
            return true;
        }

        return false;
    }

    public function isCoordinatesTaken(Coordinates $coordinates) : bool
    {
        return isset($this->takenCoordinates[$coordinates->getRepresentation()]);
    }

    public function freeCoordinates(Coordinates $coordinates) : void
    {
        unset($this->takenCoordinates[$coordinates->getRepresentation()]);
    }

    public function takeCoordinates(Coordinates $coordinates) : void
    {
        $this->takenCoordinates[$coordinates->getRepresentation()] = true;
    }
}
