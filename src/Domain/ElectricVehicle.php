<?php

declare(strict_types = 1);

namespace Kata\Domain;

use Kata\Domain\Exceptions\ElectricVehicleInitiationCoordinatesAlreadyTakenException;
use Kata\Domain\Exceptions\ElectricVehicleInitiationCoordinatesNotInBoundException;
use Kata\Domain\Exceptions\ElectricVehicleMoveForwardCoordinatesAlreadyTakenException;
use Kata\Domain\Exceptions\ElectricVehicleMoveForwardCoordinatesNotInBoundException;
use Kata\Domain\Orientation\OrientationInterface;

class ElectricVehicle
{
    /** @var Coordinates */
    private $coordinates;
    /** @var OrientationInterface */
    private $orientation;
    /** @var Area */
    private $area;

    public function __construct(Coordinates $coordinates, OrientationInterface $orientation, Area $area)
    {
        $this->area = $area;
        if ($this->area->isCoordinatesInBound($coordinates) === false) {
            throw new ElectricVehicleInitiationCoordinatesNotInBoundException();
        }
        if ($this->area->isCoordinatesTaken($coordinates)) {
            throw new ElectricVehicleInitiationCoordinatesAlreadyTakenException();
        }

        $this->coordinates = $coordinates;
        $this->orientation = $orientation;
        $this->area->takeCoordinates($this->coordinates);
    }

    public function getPosition() : string
    {
        return sprintf('%s %s', $this->coordinates->getRepresentation(), $this->orientation->getRepresentation());
    }

    /**
     * EV delegates rotation to Orientations. They better know how to rotate themselves
     */
    public function rotateLeft() : void
    {
        $this->orientation = $this->orientation->rotateLeft();
    }

    public function rotateRight() : void
    {
        $this->orientation = $this->orientation->rotateRight();
    }

    /**
     * 1 Coordinates calculate next possible coordinates according to the orientation
     * 2. Area checks if calculated coordinates is within bound and free
     * 3. Area sets current coordinates free
     * 4. Area sets next coordinates taken
     * 5. by setting next possible coordinates to EV coordinates we move EV forward
     *
     * @throws ElectricVehicleMoveForwardCoordinatesAlreadyTakenException
     * @throws ElectricVehicleMoveForwardCoordinatesNotInBoundException
     */
    public function moveForward() : void
    {
        $nextCoordinates = $this->coordinates->getNextCoordinatesAccordingToOrientation($this->orientation);

        if ($this->area->isCoordinatesInBound($nextCoordinates) === false) {
            throw new ElectricVehicleMoveForwardCoordinatesNotInBoundException();
        }

        if ($this->area->isCoordinatesTaken($nextCoordinates)) {
            throw new ElectricVehicleMoveForwardCoordinatesAlreadyTakenException();
        }

        $this->area->freeCoordinates($this->coordinates);
        $this->area->takeCoordinates($nextCoordinates);
        $this->coordinates = $nextCoordinates;
    }

    public function __toString() : string
    {
        return $this->getPosition();
    }
}
