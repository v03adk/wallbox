<?php

declare(strict_types = 1);

namespace Kata\Domain\Orientation;

class OrientationEast implements OrientationInterface
{
    public function getRepresentation() : string
    {
        return self::ORIENTATION_EAST;
    }

    public function rotateLeft() : OrientationInterface
    {
        return OrientationFactory::create(self::ORIENTATION_NORTH);
    }

    public function rotateRight() : OrientationInterface
    {
        return OrientationFactory::create(self::ORIENTATION_SOUTH);
    }
}