<?php

declare(strict_types = 1);

namespace Kata\Domain\Orientation;

class OrientationWest implements OrientationInterface
{
    public function getRepresentation() : string
    {
        return self::ORIENTATION_WEST;
    }

    public function rotateLeft() : OrientationInterface
    {
        return OrientationFactory::create(self::ORIENTATION_SOUTH);
    }

    public function rotateRight() : OrientationInterface
    {
        return OrientationFactory::create(self::ORIENTATION_NORTH);
    }
}