<?php

declare(strict_types = 1);

namespace Kata\Domain\Orientation;

interface OrientationInterface
{
    public const ORIENTATION_NORTH = 'N';
    public const ORIENTATION_EAST = 'E';
    public const ORIENTATION_SOUTH = 'S';
    public const ORIENTATION_WEST = 'W';
    public const AVAILABLE_ORIENTATIONS = [
        self::ORIENTATION_WEST,
        self::ORIENTATION_SOUTH,
        self::ORIENTATION_EAST,
        self::ORIENTATION_NORTH,
    ];

    public function getRepresentation();
    public function rotateLeft();
    public function rotateRight();
}
