<?php

declare(strict_types = 1);

namespace Kata\Domain\Orientation;

use Kata\Domain\Exceptions\OrientationCreateInvalidRepresentationException;

class OrientationFactory
{
    public static function create(string $orientation) : OrientationInterface
    {
        switch ($orientation) {
            case OrientationInterface::ORIENTATION_NORTH:
                return new OrientationNorth();
            case OrientationInterface::ORIENTATION_EAST:
                return new OrientationEast();
            case OrientationInterface::ORIENTATION_SOUTH:
                return new OrientationSouth();
            case OrientationInterface::ORIENTATION_WEST:
                return new OrientationWest();
        }

        throw new OrientationCreateInvalidRepresentationException();
    }
}
