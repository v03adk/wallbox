<?php

declare(strict_types = 1);

namespace Kata\Domain\Orientation;

class OrientationSouth implements OrientationInterface
{
    public function getRepresentation() : string
    {
        return self::ORIENTATION_SOUTH;
    }

    public function rotateLeft() : OrientationInterface
    {
        return OrientationFactory::create(self::ORIENTATION_EAST);
    }

    public function rotateRight() : OrientationInterface
    {
        return OrientationFactory::create(self::ORIENTATION_WEST);
    }
}