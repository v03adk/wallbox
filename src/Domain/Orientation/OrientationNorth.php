<?php

declare(strict_types = 1);

namespace Kata\Domain\Orientation;

class OrientationNorth implements OrientationInterface
{
    public function getRepresentation() : string
    {
        return self::ORIENTATION_NORTH;
    }

    public function rotateLeft() : OrientationInterface
    {
        return OrientationFactory::create(self::ORIENTATION_WEST);
    }

    public function rotateRight() : OrientationInterface
    {
        return OrientationFactory::create(self::ORIENTATION_EAST);
    }
}
