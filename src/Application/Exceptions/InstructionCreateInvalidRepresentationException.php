<?php

declare(strict_types=1);

namespace Kata\Application\Exceptions;

class InstructionCreateInvalidRepresentationException extends \Exception
{
    protected $message = 'Invalid representation to create an instruction';
}
