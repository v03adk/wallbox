<?php

# Symfony command to get users input: area coordinates, evs positions/instructions
# the main method is execute
# when all information is gathered and validated (only syntax) the control goes to Manager service to create Area and EVs and manage navigation


declare(strict_types = 1);

namespace Kata\Application\Service;

use Kata\Application\InputValidator\AreaCoordinatesValidator;
use Kata\Application\InputValidator\ElectricVehicleInstructionsValidator;
use Kata\Application\InputValidator\ElectricVehiclePositionValidator;
use Kata\Domain\Coordinates;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

class GetInput extends Command
{
    protected static $defaultName = 'run';

    protected const AREA_COORDINATES_QUESTION = 'Please provide the upper-right coordinates of the area';
    protected const CONTINUE_QUESTION = 'Continue adding Electric Vehicles? [y,Y => Yes, n,N,Enter => No]';
    protected const ELECTRIC_VEHICLE_POSITION_QUESTION = 'Please provide the position of the EV #';
    protected const ELECTRIC_VEHICLE_INSTRUCTIONS_QUESTION = 'Please provide the instructions for the EV #';

    protected function configure() : void
    {
        $this
            ->setDescription('get input from the command line')
            ->setHelp('This command allows you to provide input for the task and then performs all instructions')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $helper = $this->getHelper('question');

        $areaCoordinates = $this->getAreaCoordinates($helper, $input, $output);

        $index = 1;
        $evInfo = [];
        $continueQuestion = new ConfirmationQuestion(self::CONTINUE_QUESTION . "\n", false);
        $continueQuestionAnswer = true;
        while ($continueQuestionAnswer) {
            $evPosition = $this->getElectricVehiclePosition($helper, $input, $output, $index);
            $evInstructions = $this->getElectricVehicleInstructions($helper, $input, $output, $index);

            $evInfo[] = ['position' => $evPosition, 'instructions' => $evInstructions];
            $continueQuestionAnswer = $helper->ask($input, $output, $continueQuestion);
            $index++;
        }

        $output->writeln(Manager::manage($areaCoordinates, $evInfo));

        return Command::SUCCESS;
    }

    private function getAreaCoordinates(QuestionHelper $helper, InputInterface $input, OutputInterface $output) : Coordinates
    {
        $question = new Question(self::AREA_COORDINATES_QUESTION . "\n");
        $areaCoordinates = $helper->ask($input, $output, $question);

        while (AreaCoordinatesValidator::validate($areaCoordinates) !== true) {
            $question = new Question('Provided coordinates are not valid!' . self::AREA_COORDINATES_QUESTION . "\n");
            $areaCoordinates = $helper->ask($input, $output, $question);
        }

        $areaCoordinates = explode(' ', $areaCoordinates);

        return new Coordinates((int)$areaCoordinates[0], (int)$areaCoordinates[1]);
    }

    private function getElectricVehiclePosition(QuestionHelper $helper, InputInterface $input, OutputInterface $output, int $index) : string
    {
        $question = new Question(self::ELECTRIC_VEHICLE_POSITION_QUESTION . "$index\n");
        $evPosition = $helper->ask($input, $output, $question);

        while (ElectricVehiclePositionValidator::validate($evPosition) !== true) {
            $question = new Question('Provided position is not valid! ' . self::ELECTRIC_VEHICLE_POSITION_QUESTION . "$index\n");
            $evPosition = $helper->ask($input, $output, $question);
        }

        return $evPosition;
    }

    private function getElectricVehicleInstructions(QuestionHelper $helper, InputInterface $input, OutputInterface $output, int $index) : string
    {
        $question = new Question(self::ELECTRIC_VEHICLE_INSTRUCTIONS_QUESTION . "$index\n");
        $evInstructions = $helper->ask($input, $output, $question);

        while (ElectricVehicleInstructionsValidator::validate($evInstructions) !== true) {
            $question = new Question('Provided instructions are not valid! ' . self::ELECTRIC_VEHICLE_INSTRUCTIONS_QUESTION . "$index\n");
            $evInstructions = $helper->ask($input, $output, $question);
        }

        return $evInstructions;
    }
}
