<?php

# just performs all instructions on the EV

declare(strict_types = 1);

namespace Kata\Application\Service;

use Kata\Application\Instruction\InstructionInterface;
use Kata\Domain\ElectricVehicle;

class Navigation
{
    /** @var InstructionInterface[] */
    private $instructions;

    public function __construct(array $instructions)
    {
        $this->instructions = $instructions;
    }

    public function navigate(ElectricVehicle $ev) : ElectricVehicle
    {
        foreach ($this->instructions as $instruction) {
            $instruction->perform($ev);
        }

        return $ev;
    }
}
