<?php

# Service creates Area, EVs and Instructions from the provided (and validated only for syntax) input
# for each EV calls Navigation->navigate method
# Manager's duty to add EV's final position to the output
# OR add an error to the output if something happened during initiation/navigation of the EV

declare(strict_types = 1);

namespace Kata\Application\Service;

use Kata\Application\Instruction\InstructionFactory;
use Kata\Domain\Area;
use Kata\Domain\Coordinates;
use Kata\Domain\ElectricVehicle;
use Kata\Domain\Orientation\OrientationFactory;

class Manager
{
    public static function manage(Coordinates $areaCoordinates, array $evInfo) : string
    {
        $output = '';
        $area = new Area($areaCoordinates);
        $index = 1;
        foreach ($evInfo as $info) {
            try {
                $instructions = [];
                $inputInstructionsArray = str_split($info['instructions']);
                foreach ($inputInstructionsArray as $instruction) {
                    $instructions[] = InstructionFactory::create($instruction);
                }
                $evPosition = explode(' ', $info['position']);
                $ev = new ElectricVehicle(new Coordinates((int)$evPosition[0], (int)$evPosition[1]), OrientationFactory::create($evPosition[2]), $area);
                $navigation = new Navigation($instructions);
                $navigation->navigate($ev);

                $output = sprintf("%sFinal position of the EV #%d: %s\n", $output, $index, $ev);
            } catch (\Exception $e) {
                $output = sprintf("%sError for EV #%d during processing: %s\n", $output, $index, $e->getMessage());
            }
            $index++;
        }

        return $output;
    }
}
