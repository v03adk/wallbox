<?php

declare(strict_types = 1);

namespace Kata\Application\InputValidator;

class AreaCoordinatesValidator
{
    public static function validate(string $coordinates) : bool
    {
        return preg_match('#^\d+\s{1}\d+$#', $coordinates) === 1;
    }
}
