<?php

declare(strict_types = 1);

namespace Kata\Application\InputValidator;

use Kata\Application\Instruction\InstructionInterface;

class ElectricVehicleInstructionsValidator
{
    public static function validate(string $instructions) : bool
    {
        return preg_match('#^['.implode('', InstructionInterface::AVAILABLE_INSTRUCTIONS).']+$#', $instructions) === 1;
    }
}
