<?php

declare(strict_types = 1);

namespace Kata\Application\InputValidator;

use Kata\Domain\Orientation\OrientationInterface;

class ElectricVehiclePositionValidator
{
    public static function validate(string $position) : bool
    {
        return preg_match('#^\d+\s{1}\d+\s{1}['.implode('', OrientationInterface::AVAILABLE_ORIENTATIONS).']{1}$#', $position) === 1;
    }
}
