<?php

declare(strict_types = 1);

namespace Kata\Application\Instruction;

use Kata\Domain\ElectricVehicle;

class InstructionRotateRight implements InstructionInterface
{
    public function perform(ElectricVehicle $ev) : void
    {
        $ev->rotateRight();
    }
}
