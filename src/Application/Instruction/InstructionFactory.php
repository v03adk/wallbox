<?php

declare(strict_types = 1);

namespace Kata\Application\Instruction;

use Kata\Application\Exceptions\InstructionCreateInvalidRepresentationException;

class InstructionFactory
{
    public static function create(string $instruction) : InstructionInterface
    {
        switch ($instruction) {
            case InstructionInterface::INSTRUCTION_MOVE_FORWARD:
                return new InstructionMoveForward();
            case InstructionInterface::INSTRUCTION_ROTATE_LEFT:
                return new InstructionRotateLeft();
            case InstructionInterface::INSTRUCTION_ROTATE_RIGHT:
                return new InstructionRotateRight();
        }

        throw new InstructionCreateInvalidRepresentationException();
    }
}
