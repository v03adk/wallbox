<?php

declare(strict_types = 1);

namespace Kata\Application\Instruction;

use Kata\Domain\ElectricVehicle;

interface InstructionInterface
{
    public const INSTRUCTION_ROTATE_LEFT = 'L';
    public const INSTRUCTION_ROTATE_RIGHT = 'R';
    public const INSTRUCTION_MOVE_FORWARD = 'M';
    public const AVAILABLE_INSTRUCTIONS = [
        self::INSTRUCTION_ROTATE_RIGHT,
        self::INSTRUCTION_ROTATE_LEFT,
        self::INSTRUCTION_MOVE_FORWARD,
    ];

    public function perform(ElectricVehicle $ev);
}
