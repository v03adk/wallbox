<?php

# Entry point of the application
# Symfony console application is created and the only command GetInput starts working
# Application\Service\GetInput handles users input to complete the backend challenge

declare(strict_types = 1);

require_once('autoload.php');

use Symfony\Component\Console\Application;
use Kata\Application\Service\GetInput;

$command = new GetInput();
$application = new Application();
$application->add($command);
$application->setDefaultCommand($command->getName(), true);
$application->run();
