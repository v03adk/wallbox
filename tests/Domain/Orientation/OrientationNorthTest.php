<?php

declare(strict_types = 1);

namespace Kata\Tests\Domain\Orientation;

use Kata\Domain\Orientation\OrientationFactory;
use Kata\Domain\Orientation\OrientationInterface;
use PHPUnit\Framework\TestCase;

final class OrientationNorthTest extends TestCase
{
    private $orientationNorth;

    protected function setUp(): void
    {
        $this->orientationNorth = OrientationFactory::create(OrientationInterface::ORIENTATION_NORTH);
    }

    public function testGetRepresentation(): void
    {
        $this->assertEquals(
            $this->orientationNorth->getRepresentation(),
            OrientationInterface::ORIENTATION_NORTH
        );
    }

    public function testRotateLeft(): void
    {
        $this->assertEquals(
            $this->orientationNorth->rotateLeft()->getRepresentation(),
            OrientationInterface::ORIENTATION_WEST
        );
    }

    public function testRotateRight(): void
    {
        $this->assertEquals(
            $this->orientationNorth->rotateRight()->getRepresentation(),
            OrientationInterface::ORIENTATION_EAST
        );
    }
}
