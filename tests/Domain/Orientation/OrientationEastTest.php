<?php

declare(strict_types = 1);

namespace Kata\Tests\Domain\Orientation;

use Kata\Domain\Orientation\OrientationFactory;
use Kata\Domain\Orientation\OrientationInterface;
use PHPUnit\Framework\TestCase;

final class OrientationEastTest extends TestCase
{
    private $orientationEast;

    protected function setUp(): void
    {
        $this->orientationEast = OrientationFactory::create(OrientationInterface::ORIENTATION_EAST);
    }

    public function testGetRepresentation(): void
    {
        $this->assertEquals(
            $this->orientationEast->getRepresentation(),
            OrientationInterface::ORIENTATION_EAST
        );
    }

    public function testRotateLeft(): void
    {
        $this->assertEquals(
            $this->orientationEast->rotateLeft()->getRepresentation(),
            OrientationInterface::ORIENTATION_NORTH
        );
    }

    public function testRotateRight(): void
    {
        $this->assertEquals(
            $this->orientationEast->rotateRight()->getRepresentation(),
            OrientationInterface::ORIENTATION_SOUTH
        );
    }
}
