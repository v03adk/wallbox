<?php

declare(strict_types = 1);

namespace Kata\Tests\Domain\Orientation;

use Kata\Domain\Exceptions\OrientationCreateInvalidRepresentationException;
use Kata\Domain\Orientation\OrientationEast;
use Kata\Domain\Orientation\OrientationFactory;
use Kata\Domain\Orientation\OrientationInterface;
use Kata\Domain\Orientation\OrientationNorth;
use Kata\Domain\Orientation\OrientationSouth;
use Kata\Domain\Orientation\OrientationWest;
use PHPUnit\Framework\TestCase;

final class OrientationFactoryTest extends TestCase
{
    public function testCreateThrowsException(): void
    {
        $this->expectException(OrientationCreateInvalidRepresentationException::class);

        OrientationFactory::create('T');
    }

    public function testCreateNorth(): void
    {
        $this->assertInstanceOf(
            OrientationNorth::class,
            OrientationFactory::create(OrientationInterface::ORIENTATION_NORTH)
        );
    }

    public function testCreateEast(): void
    {
        $this->assertInstanceOf(
            OrientationEast::class,
            OrientationFactory::create(OrientationInterface::ORIENTATION_EAST)
        );
    }

    public function testCreateSouth(): void
    {
        $this->assertInstanceOf(
            OrientationSouth::class,
            OrientationFactory::create(OrientationInterface::ORIENTATION_SOUTH)
        );
    }

    public function testCreateWest(): void
    {
        $this->assertInstanceOf(
            OrientationWest::class,
            OrientationFactory::create(OrientationInterface::ORIENTATION_WEST)
        );
    }
}
