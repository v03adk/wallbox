<?php

declare(strict_types = 1);

namespace Kata\Tests\Domain\Orientation;

use Kata\Domain\Orientation\OrientationFactory;
use Kata\Domain\Orientation\OrientationInterface;
use PHPUnit\Framework\TestCase;

final class OrientationSouthTest extends TestCase
{
    private $orientationSouth;

    protected function setUp(): void
    {
        $this->orientationSouth = OrientationFactory::create(OrientationInterface::ORIENTATION_SOUTH);
    }

    public function testGetRepresentation(): void
    {
        $this->assertEquals(
            $this->orientationSouth->getRepresentation(),
            OrientationInterface::ORIENTATION_SOUTH
        );
    }

    public function testRotateLeft(): void
    {
        $this->assertEquals(
            $this->orientationSouth->rotateLeft()->getRepresentation(),
            OrientationInterface::ORIENTATION_EAST
        );
    }

    public function testRotateRight(): void
    {
        $this->assertEquals(
            $this->orientationSouth->rotateRight()->getRepresentation(),
            OrientationInterface::ORIENTATION_WEST
        );
    }
}
