<?php

declare(strict_types = 1);

namespace Kata\Tests\Domain\Orientation;

use Kata\Domain\Orientation\OrientationFactory;
use Kata\Domain\Orientation\OrientationInterface;
use PHPUnit\Framework\TestCase;

final class OrientationWestTest extends TestCase
{
    private $orientationWest;

    protected function setUp(): void
    {
        $this->orientationWest = OrientationFactory::create(OrientationInterface::ORIENTATION_WEST);
    }

    public function testGetRepresentation(): void
    {
        $this->assertEquals(
            $this->orientationWest->getRepresentation(),
            OrientationInterface::ORIENTATION_WEST
        );
    }

    public function testRotateLeft(): void
    {
        $this->assertEquals(
            $this->orientationWest->rotateLeft()->getRepresentation(),
            OrientationInterface::ORIENTATION_SOUTH
        );
    }

    public function testRotateRight(): void
    {
        $this->assertEquals(
            $this->orientationWest->rotateRight()->getRepresentation(),
            OrientationInterface::ORIENTATION_NORTH
        );
    }
}
