<?php

declare(strict_types = 1);

namespace Kata\Tests\Domain;

use Kata\Domain\Coordinates;
use Kata\Domain\Orientation\OrientationFactory;
use Kata\Domain\Orientation\OrientationInterface;
use PHPUnit\Framework\TestCase;

final class CoordinatesTest extends TestCase
{
    public function testCanBeCreatedFromValidData(): void
    {
        $this->assertInstanceOf(
            Coordinates::class,
            new Coordinates(5 ,5)
        );
    }

    public function testGetRepresentation(): void
    {
        $coordinates = new Coordinates(5, 6);
        $this->assertEquals(
            '5 6',
            $coordinates->getRepresentation()
        );
    }

    public function testGetNextCoordinatesAccordingToOrientation(): void
    {
        $coordinates = new Coordinates(1, 1);
        $orientationNorth = OrientationFactory::create(OrientationInterface::ORIENTATION_NORTH);
        $orientationEast = OrientationFactory::create(OrientationInterface::ORIENTATION_EAST);
        $orientationSouth = OrientationFactory::create(OrientationInterface::ORIENTATION_SOUTH);
        $orientationWest = OrientationFactory::create(OrientationInterface::ORIENTATION_WEST);

        $this->assertEquals(
            '1 2',
            $coordinates->getNextCoordinatesAccordingToOrientation($orientationNorth)->getRepresentation()
        );

        $this->assertEquals(
            '2 1',
            $coordinates->getNextCoordinatesAccordingToOrientation($orientationEast)->getRepresentation()
        );

        $this->assertEquals(
            '1 0',
            $coordinates->getNextCoordinatesAccordingToOrientation($orientationSouth)->getRepresentation()
        );

        $this->assertEquals(
            '0 1',
            $coordinates->getNextCoordinatesAccordingToOrientation($orientationWest)->getRepresentation()
        );
    }
}
