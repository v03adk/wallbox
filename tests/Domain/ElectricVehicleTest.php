<?php

declare(strict_types = 1);

namespace Kata\Tests\Domain;

use Kata\Domain\Area;
use Kata\Domain\Coordinates;
use Kata\Domain\ElectricVehicle;
use Kata\Domain\Exceptions\ElectricVehicleInitiationCoordinatesAlreadyTakenException;
use Kata\Domain\Exceptions\ElectricVehicleInitiationCoordinatesNotInBoundException;
use Kata\Domain\Exceptions\ElectricVehicleMoveForwardCoordinatesAlreadyTakenException;
use Kata\Domain\Exceptions\ElectricVehicleMoveForwardCoordinatesNotInBoundException;
use Kata\Domain\Orientation\OrientationFactory;
use Kata\Domain\Orientation\OrientationInterface;
use PHPUnit\Framework\TestCase;

final class ElectricVehicleTest extends TestCase
{
    private $basicEv;

    protected function setUp(): void
    {
        $this->basicEv = new ElectricVehicle(
            new Coordinates(2, 2),
            OrientationFactory::create(OrientationInterface::ORIENTATION_NORTH),
            new Area(new Coordinates(5, 5))
        );
    }

    public function testConstructThrowsCoordinatesNotInBoundException(): void
    {
        $this->expectException(ElectricVehicleInitiationCoordinatesNotInBoundException::class);

        $ev = new ElectricVehicle(new Coordinates(6, 6), OrientationFactory::create(OrientationInterface::ORIENTATION_NORTH), new Area(new Coordinates(5, 5)));
    }

    public function testConstructThrowsCoordinatesAlreadyTakenException(): void
    {
        $this->expectException(ElectricVehicleInitiationCoordinatesAlreadyTakenException::class);

        $area = new Area(new Coordinates(5, 5));
        $area->takeCoordinates(new Coordinates(3, 3));

        $ev = new ElectricVehicle(new Coordinates(3, 3), OrientationFactory::create(OrientationInterface::ORIENTATION_NORTH), $area);
    }

    public function testGetPosition(): void
    {
        $this->assertEquals('2 2 ' . OrientationInterface::ORIENTATION_NORTH, $this->basicEv->getPosition());
    }

    public function testRotateLeft(): void
    {
        $this->basicEv->rotateLeft();

        $this->assertEquals('2 2 ' . OrientationInterface::ORIENTATION_WEST, $this->basicEv->getPosition());
    }

    public function testRotateRight(): void
    {
        $this->basicEv->rotateRight();

        $this->assertEquals('2 2 ' . OrientationInterface::ORIENTATION_EAST, $this->basicEv->getPosition());
    }

    public function testMoveForward(): void
    {
        $this->basicEv->moveForward();

        $this->assertEquals('2 3 ' . OrientationInterface::ORIENTATION_NORTH, $this->basicEv->getPosition());

        $this->basicEv->moveForward();

        $this->assertEquals('2 4 ' . OrientationInterface::ORIENTATION_NORTH, $this->basicEv->getPosition());

        $this->basicEv->moveForward();

        $this->assertEquals('2 5 ' . OrientationInterface::ORIENTATION_NORTH, $this->basicEv->getPosition());

        $this->expectException(ElectricVehicleMoveForwardCoordinatesNotInBoundException::class);

        $this->basicEv->moveForward();

        $this->basicEv->rotateRight();
        $this->basicEv->rotateRight();
        $this->basicEv->moveForward();

        $this->assertEquals('2 4 ' . OrientationInterface::ORIENTATION_NORTH, $this->basicEv->getPosition());
    }

    public function testMoveForwardThrowsCoordinatesAlreadyTakenException(): void
    {
        $area = new Area(new Coordinates(5, 5));
        $ev = new ElectricVehicle(new Coordinates(2, 2), OrientationFactory::create(OrientationInterface::ORIENTATION_NORTH), $area);

        $anotherEv = new ElectricVehicle(new Coordinates(2, 1), OrientationFactory::create(OrientationInterface::ORIENTATION_NORTH), $area);

        $this->expectException(ElectricVehicleMoveForwardCoordinatesAlreadyTakenException::class);

        $anotherEv->moveForward();
    }
}
