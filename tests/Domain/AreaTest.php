<?php

declare(strict_types = 1);

namespace Kata\Tests\Domain;

use Kata\Domain\Area;
use Kata\Domain\Coordinates;
use PHPUnit\Framework\TestCase;

final class AreaTest extends TestCase
{
    public function testCanBeCreatedFromValidData(): void
    {
        $this->assertInstanceOf(
            Area::class,
            new Area(new Coordinates(5, 5))
        );
    }

    public function testIsCoordinatesInBound(): void
    {
        $area = new Area(new Coordinates(6, 6));

        $this->assertTrue($area->isCoordinatesInBound(new Coordinates(4, 4)));
        $this->assertTrue($area->isCoordinatesInBound(new Coordinates(6, 6)));
        $this->assertTrue($area->isCoordinatesInBound(new Coordinates(0, 0)));

        $this->assertFalse($area->isCoordinatesInBound(new Coordinates(7, 7)));
        $this->assertFalse($area->isCoordinatesInBound(new Coordinates(0, -1)));
        $this->assertFalse($area->isCoordinatesInBound(new Coordinates(-1, -1)));
        $this->assertFalse($area->isCoordinatesInBound(new Coordinates(6, 7)));
        $this->assertFalse($area->isCoordinatesInBound(new Coordinates(7, 6)));
    }

    public function testTakeFreeIsTaken(): void
    {
        $area = new Area(new Coordinates(6, 6));

        $area->takeCoordinates(new Coordinates(3, 4));
        $this->assertTrue($area->isCoordinatesTaken(new Coordinates(3, 4)));
        $area->freeCoordinates(new Coordinates(3, 4));
        $this->assertFalse($area->isCoordinatesTaken(new Coordinates(3, 4)));
    }
}
