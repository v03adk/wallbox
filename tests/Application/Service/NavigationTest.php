<?php

declare(strict_types=1);

namespace Kata\Tests\Application\Service;

use Kata\Application\Instruction\InstructionFactory;
use Kata\Application\Instruction\InstructionInterface;
use Kata\Application\Service\Navigation;
use Kata\Domain\Area;
use Kata\Domain\Coordinates;
use Kata\Domain\ElectricVehicle;
use Kata\Domain\Orientation\OrientationFactory;
use Kata\Domain\Orientation\OrientationInterface;
use PHPUnit\Framework\TestCase;

final class NavigationTest extends TestCase
{
    protected $ev;

    public function setUp(): void
    {
        $this->ev = new ElectricVehicle(
            new Coordinates(2, 2),
            OrientationFactory::create(OrientationInterface::ORIENTATION_NORTH),
            new Area(new Coordinates(5, 5))
        );
    }

    public function testNavigateFourRights(): void
    {
        $instructions = [
            InstructionFactory::create(InstructionInterface::INSTRUCTION_ROTATE_RIGHT),
            InstructionFactory::create(InstructionInterface::INSTRUCTION_ROTATE_RIGHT),
            InstructionFactory::create(InstructionInterface::INSTRUCTION_ROTATE_RIGHT),
            InstructionFactory::create(InstructionInterface::INSTRUCTION_ROTATE_RIGHT),
        ];

        $navigation = new Navigation($instructions);
        $navigation->navigate($this->ev);

        $this->assertEquals('2 2 N', $this->ev->getPosition());
    }

    public function testNavigateFourLefts(): void
    {
        $instructions = [
            InstructionFactory::create(InstructionInterface::INSTRUCTION_ROTATE_LEFT),
            InstructionFactory::create(InstructionInterface::INSTRUCTION_ROTATE_LEFT),
            InstructionFactory::create(InstructionInterface::INSTRUCTION_ROTATE_LEFT),
            InstructionFactory::create(InstructionInterface::INSTRUCTION_ROTATE_LEFT),
        ];

        $navigation = new Navigation($instructions);
        $navigation->navigate($this->ev);

        $this->assertEquals('2 2 N', $this->ev->getPosition());
    }

    public function testNavigateMoveForward(): void
    {
        $instructions = [
            InstructionFactory::create(InstructionInterface::INSTRUCTION_MOVE_FORWARD),
        ];

        $navigation = new Navigation($instructions);
        $navigation->navigate($this->ev);

        $this->assertEquals('2 3 N', $this->ev->getPosition());
    }

    public function testNavigateTurnAroundAndOneMoveForward(): void
    {
        $instructions = [
            InstructionFactory::create(InstructionInterface::INSTRUCTION_ROTATE_RIGHT),
            InstructionFactory::create(InstructionInterface::INSTRUCTION_ROTATE_RIGHT),
            InstructionFactory::create(InstructionInterface::INSTRUCTION_MOVE_FORWARD),
        ];

        $navigation = new Navigation($instructions);
        $navigation->navigate($this->ev);

        $this->assertEquals('2 1 S', $this->ev->getPosition());
    }

    public function testNavigateRotateButStillMoveForward(): void
    {
        $instructions = [
            InstructionFactory::create(InstructionInterface::INSTRUCTION_ROTATE_RIGHT),
            InstructionFactory::create(InstructionInterface::INSTRUCTION_ROTATE_LEFT),
            InstructionFactory::create(InstructionInterface::INSTRUCTION_MOVE_FORWARD),
        ];

        $navigation = new Navigation($instructions);
        $navigation->navigate($this->ev);

        $this->assertEquals('2 3 N', $this->ev->getPosition());
    }
}
