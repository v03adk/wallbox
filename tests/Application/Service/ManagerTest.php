<?php

declare(strict_types=1);

namespace Kata\Tests\Application\Service;

use Kata\Application\Service\Manager;
use Kata\Domain\Coordinates;
use PHPUnit\Framework\TestCase;

final class ManagerTest extends TestCase
{
    public function testManage(): void
    {
        $evInfo = [
            ['position' => '1 2 N', 'instructions' => 'LMLMLMLMM'],
            ['position' => '3 3 E', 'instructions' => 'MMRMMRMRRM'],
        ];

        $this->assertEquals(
            "Final position of the EV #1: 1 3 N\nFinal position of the EV #2: 5 1 E\n",
            Manager::manage(new Coordinates(5, 5), $evInfo)
        );
    }
}
