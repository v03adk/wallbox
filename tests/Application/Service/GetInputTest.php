<?php

declare(strict_types=1);

namespace Kata\Tests\Application\Service;

use Kata\Application\Service\GetInput;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\ApplicationTester;

final class GetInputTest extends TestCase
{
    public function testExecute(): void
    {
        $application = new Application();
        $command = new GetInput();
        $application->add($command);
        $application->setDefaultCommand($command->getName(), true);
        $application->setAutoExit(false);

        $tester = new ApplicationTester($application);

        $tester->setInputs(['5 5', '1 2 N', 'LMLMLMLMM', 'N']);

        $tester->run([]);

        $this->assertEquals(
            "Please provide the upper-right coordinates of the area\nPlease provide the position of the EV #1\nPlease provide the instructions for the EV #1\nContinue adding Electric Vehicles? [y,Y => Yes, n,N,Enter => No]\nFinal position of the EV #1: 1 3 N\n\n",
            $tester->getDisplay());
    }
}
