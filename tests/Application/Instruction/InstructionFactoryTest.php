<?php

declare(strict_types=1);

namespace Kata\Tests\Application\Instruction;

use Kata\Application\Exceptions\InstructionCreateInvalidRepresentationException;
use Kata\Application\Instruction\InstructionFactory;
use Kata\Application\Instruction\InstructionInterface;
use Kata\Application\Instruction\InstructionMoveForward;
use Kata\Application\Instruction\InstructionRotateLeft;
use Kata\Application\Instruction\InstructionRotateRight;
use PHPUnit\Framework\TestCase;

final class InstructionFactoryTest extends TestCase
{
    public function testCreateThrowsException(): void
    {
        $this->expectException(InstructionCreateInvalidRepresentationException::class);

        InstructionFactory::create('W');
    }

    public function testCreateRotateLeft(): void
    {
        $this->assertInstanceOf(
            InstructionRotateLeft::class,
            InstructionFactory::create(InstructionInterface::INSTRUCTION_ROTATE_LEFT)
        );
    }

    public function testCreateRotateRight(): void
    {
        $this->assertInstanceOf(
            InstructionRotateRight::class,
            InstructionFactory::create(InstructionInterface::INSTRUCTION_ROTATE_RIGHT)
        );
    }

    public function testCreateMoveForward(): void
    {
        $this->assertInstanceOf(
            InstructionMoveForward::class,
            InstructionFactory::create(InstructionInterface::INSTRUCTION_MOVE_FORWARD)
        );
    }
}
