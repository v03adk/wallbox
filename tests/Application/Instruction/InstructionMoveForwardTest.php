<?php

declare(strict_types=1);

namespace Kata\Tests\Application\Instruction;

use Kata\Application\Instruction\InstructionFactory;
use Kata\Application\Instruction\InstructionInterface;
use Kata\Domain\Area;
use Kata\Domain\Coordinates;
use Kata\Domain\ElectricVehicle;
use Kata\Domain\Orientation\OrientationFactory;
use Kata\Domain\Orientation\OrientationInterface;
use PHPUnit\Framework\TestCase;

final class InstructionMoveForwardTest extends TestCase
{
    public function testPerform(): void
    {
        $instructionMoveForward = InstructionFactory::create(InstructionInterface::INSTRUCTION_MOVE_FORWARD);
        $ev = new ElectricVehicle(
            new Coordinates(2, 2),
            OrientationFactory::create(OrientationInterface::ORIENTATION_NORTH),
            new Area(new Coordinates(5, 5))
        );

        $instructionMoveForward->perform($ev);

        $this->assertEquals('2 3 N', $ev->getPosition());
    }
}