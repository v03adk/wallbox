<?php

declare(strict_types=1);

namespace Kata\Tests\Application\Instruction;

use Kata\Application\Instruction\InstructionFactory;
use Kata\Application\Instruction\InstructionInterface;
use Kata\Domain\Area;
use Kata\Domain\Coordinates;
use Kata\Domain\ElectricVehicle;
use Kata\Domain\Orientation\OrientationFactory;
use Kata\Domain\Orientation\OrientationInterface;
use PHPUnit\Framework\TestCase;

final class InstructionRotateLeftTest extends TestCase
{
    public function testPerform(): void
    {
        $instructionRotateLeft = InstructionFactory::create(InstructionInterface::INSTRUCTION_ROTATE_LEFT);
        $ev = new ElectricVehicle(
            new Coordinates(2, 2),
            OrientationFactory::create(OrientationInterface::ORIENTATION_NORTH),
            new Area(new Coordinates(5, 5))
        );

        $instructionRotateLeft->perform($ev);

        $this->assertEquals('2 2 W', $ev->getPosition());
    }
}