<?php

declare(strict_types = 1);

namespace Kata\Tests\Application\InputValidator;

use Kata\Application\InputValidator\ElectricVehicleInstructionsValidator;
use PHPUnit\Framework\TestCase;

final class ElectricVehicleInstructionsValidatorTest extends TestCase
{
    public function testValidate(): void
    {
        $this->assertTrue(ElectricVehicleInstructionsValidator::validate('M'));
        $this->assertTrue(ElectricVehicleInstructionsValidator::validate('MLR'));
        $this->assertTrue(ElectricVehicleInstructionsValidator::validate('LLLL'));
        $this->assertTrue(ElectricVehicleInstructionsValidator::validate('MMRR'));

        $this->assertFalse(ElectricVehicleInstructionsValidator::validate(''));
        $this->assertFalse(ElectricVehicleInstructionsValidator::validate('rrr'));
        $this->assertFalse(ElectricVehicleInstructionsValidator::validate('lMR'));
        $this->assertFalse(ElectricVehicleInstructionsValidator::validate('LLLLL1'));
    }
}
