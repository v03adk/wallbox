<?php

declare(strict_types = 1);

namespace Kata\Tests\Application\InputValidator;

use Kata\Application\InputValidator\AreaCoordinatesValidator;
use PHPUnit\Framework\TestCase;

final class AreaCoordinatesValidatorTest extends TestCase
{
    public function testValidate(): void
    {
        $this->assertTrue(AreaCoordinatesValidator::validate('0 0'));
        $this->assertTrue(AreaCoordinatesValidator::validate('5 5'));
        $this->assertTrue(AreaCoordinatesValidator::validate('10 100'));

        $this->assertFalse(AreaCoordinatesValidator::validate(''));
        $this->assertFalse(AreaCoordinatesValidator::validate('-1 -1'));
        $this->assertFalse(AreaCoordinatesValidator::validate('5  5'));
        $this->assertFalse(AreaCoordinatesValidator::validate('55'));
        $this->assertFalse(AreaCoordinatesValidator::validate('0 o'));
        $this->assertFalse(AreaCoordinatesValidator::validate(' 5 5'));
        $this->assertFalse(AreaCoordinatesValidator::validate('-5 5'));
    }
}
