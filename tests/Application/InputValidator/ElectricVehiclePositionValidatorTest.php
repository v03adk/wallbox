<?php

declare(strict_types = 1);

namespace Kata\Tests\Application\InputValidator;

use Kata\Application\InputValidator\ElectricVehiclePositionValidator;
use PHPUnit\Framework\TestCase;

final class ElectricVehiclePositionValidatorTest extends TestCase
{
    public function testValidate(): void
    {
        $this->assertTrue(ElectricVehiclePositionValidator::validate('0 0 S'));
        $this->assertTrue(ElectricVehiclePositionValidator::validate('2 5 N'));
        $this->assertTrue(ElectricVehiclePositionValidator::validate('1 1 W'));
        $this->assertTrue(ElectricVehiclePositionValidator::validate('1 5 E'));

        $this->assertFalse(ElectricVehiclePositionValidator::validate(''));
        $this->assertFalse(ElectricVehiclePositionValidator::validate('0 0'));
        $this->assertFalse(ElectricVehiclePositionValidator::validate('0 0 K'));
        $this->assertFalse(ElectricVehiclePositionValidator::validate(' 0 0 S'));
        $this->assertFalse(ElectricVehiclePositionValidator::validate('1 1  N'));
    }
}
