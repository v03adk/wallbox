## Install and run
- Go to the root directory and run `composer install`
- run application from the src directory `php app.php`
- run tests from the root directory `./vendor/bin/phpunit tests`

## Implementation
Symfony standalone console component, interactive command that asks for area coordinates, EV position/instructions.

As it's a simple test assignment I tried to not complicate implementation:
- One context for Domain
- Two layers: Domain and Application (Domain is inner, doesn't depend on Application)
- Minimal set of unit/functional tests



`src\app.php` is the entry point of the application.
Symfony console application is created and the only command GetInput starts working
Application\Service\GetInput handles users input to complete the backend challenge

`src\Application\Service\GetInput.php`
Symfony command to get users input: area coordinates, evs positions/instructions
the main method is execute
when all information is gathered and validated (only syntax) the control goes to Manager service to create Area and EVs and manage navigation

`src\Application\Service\Manager.php`
Service creates Area, EVs and Instructions from the provided (and validated only for syntax) input
for each EV calls Navigation->navigate method
Manager's duty to add EV's final position to the output
OR add an error to the output if something happened during initiation/navigation of the EV

`src\Domain\ElectricVehicle.php` contains some comments about methods implementation.


